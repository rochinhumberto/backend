# backend

Nodejs backend test

Proyecto para la realización de operaciones aritméticas con los elementos de un arreglo.

Para correr el proyecto solo se necesita terner instalado nodejs en el equipo descargar el proyecto y correrlo con el comando npm start, 
éste se levantará en el puerto 3000.

La aplicacion cuenta con un endpoint "/test" el cual recibe un JSON que debe contener un arreglo de números, valida los datos y realiza la suma, resta, 
multiplicación y división de todos los elementos, (para la división se toma como base el primer número del arreglo)

EJEMPLOS:

JSON a enviar:
{
	"array":[1,2,3]
}

JSON de respuesta:
{
	"data":{
		"suma":6,
		"resta":-6,
		"multiplicacion":6,
		"division":0.1666666666
	},
	"errors":[]
}


Para el desarrollo de las pruebas se usó Mocha y Chai con el siguiente script: (se agregan comentarios de cada caso de prueba)

	'use strict'
	var chai = require('chai');
	var chaiHttp = require('chai-http');
	var expect = require('chai').expect;
	var should = chai.should();

	chai.use(chaiHttp);

	var url = "http://localhost:3000"

	describe('TEST SERVICE',()=>{

		//SE ENVIA ARREGLO DE 3 NUMEROS RESPONDE CON STATUS 200
		it('status 200 everything ok', (done) => {
			chai.request(url)
			.post('/test')
			.send({array:[8,2,2]})
			.end( function(err,res){
				//console.log(err); 
				//console.log(res.body)
				expect(res).to.have.status(200);	

				done();
			});
		});
		
		//SE ENVIA ARREGLO CON UN 0 DESPUES DE LA PRIMER POSICION RESPONDE CON STATUS 200 Y LOS RESULTADOS DE LAS 
		//OPERACIONES A EXCEPCION QUE EN LA DIVISION DA NULL Y SE AGREGA A ERRORES QUE NO SE PUEDE DIVIRIR ENTRE 0
		it('status 200 error in division', (done) => {
			chai.request(url)
			.post('/test')
			.send({array:[8,2,0,2]})
			.end( function(err,res){
				//console.log(err); 
				//console.log(res.body)
				expect(res).to.have.status(200);

				var checkObj = {
					data: {
						suma: 12,
						resta: -12,
						multiplicacion: 0,
						division: null
					},
					errors: ["can_not_divide_by_zero"]
				}
				res.body.should.be.eql(checkObj); // passes test

				done();
			});
		});
		
		//SE ENVIA INCORRECTO EL ARREGLO
		it('status 422 error in data', (done) => {
			chai.request(url)
			.post('/test')
			.send({araray:[8,2,2]})
			.end( function(err,res){
				//console.log(err); 
				//console.log(res.body)
				expect(res).to.have.status(422);

				done();
			});
		});
		
		//NO SE ENVIA NADA
		it('status 422 error in data', (done) => {
			chai.request(url)
			.post('/test')
			.send({})
			.end( function(err,res){
				//console.log(err); 
				//console.log(res.body)
				expect(res).to.have.status(422);

				done();
			});
		});
		
		//SE INTENTA CONSUMIR UNA URL NO EXISTENTE
		it('status 404 wrong url', (done) => {
			chai.request(url)
			.post('/testa')
			.send({araray:[8,2,2]})
			.end( function(err,res){
				//console.log(err); 
				//console.log(res.body)
				expect(res).to.have.status(404);

				done();
			});
		});
	  
	});