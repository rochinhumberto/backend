'use strict';
var express = require( 'express' );
var router  = express.Router();

var controller = require( '../controllers/controller' );

router
    .post( '/test', controller.operations )

module.exports = router;