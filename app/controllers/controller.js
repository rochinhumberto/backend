'use strict';

exports.operations = (req, res, next) => {

    var numbersArray = req.body.array;

	var jsonResponse = { data: {}, errors: [] };

	if(numbersArray != undefined && Array.isArray(numbersArray)) //valida que venga array en el json y que sea un arreglo 
	{
		if(numbersArray.length > 0)
		{
			if(!numbersArray.some(isNaN)) //valida que los datos contenidos en el array sean numeros
			{
				var data = {
					suma: numbersArray.reduce( (a, b) => Number(a) + Number(b) ),
					resta: numbersArray.reduce( (a, b) => a - b ,0),
					multiplicacion: numbersArray.reduce( (a, b) => a * b ),
					division: numbersArray.reduce( (a, b) => a / b),
				};

				jsonResponse.data = data;
				//si algun numero despues del primero dentro del arreglo es igual a cero la division no se puede realizar(nos da infinity), si el primer numero tambien es cero nos da NaN
				if(isNaN(data.division) || data.division == Infinity)
				{
					jsonResponse.errors.push("can_not_divide_by_zero");
				}
				res.status(200).json(jsonResponse);
			}
			else
			{
				jsonResponse.errors.push("invalid_data_format");
				jsonResponse.errors.push("all_elements_should_be_numbers");
				res.status(422).json(jsonResponse);
			}
		}
		else
		{
			jsonResponse.errors.push("invalid_data_format");
			jsonResponse.errors.push("empty_array");
			res.status(422).json(jsonResponse);
		}
				
	}
	else
	{
		jsonResponse.errors.push("invalid_data_format");
    	res.status(422).json(jsonResponse);
	}

}
