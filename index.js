'use strict'
var express 	= require('express');
var bodyParser  = require('body-parser')
var app 		= express();

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

var routes = require('./app/routes/routes.js')
app.use(routes);

app.use(( req, res, next ) => {
    const error = new Error('not_found');
    error.status = 404;
    next(error);
});

//errores
app.use(( error, req, res, next ) => {
    res.status(error.status || 500);
    res.json({ data: "", errors: [error.message] });
});

app.listen(3000, function () {
	console.log('app listening on port 3000!');
});

